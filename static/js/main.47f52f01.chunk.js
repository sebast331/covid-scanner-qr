(this["webpackJsonpakinox-tsp-mobile-app"] =
  this["webpackJsonpakinox-tsp-mobile-app"] || []).push([
  [0],
  {
    216: function (e) {
      e.exports = JSON.parse(
        '{"APP":{"BIRTHDATE":"Date of birth","DEVICE_LOAD_ERROR":"Error loading camera device. Please reload the application.","FEMALE":"Woman","GENDER":"Gender","LOCATION":"Immunization Provider or Clinic Site","LOT_NUMBER":"Lot","MALE":"Man","NAME":"Name","OCCURRENCE_DATE_TIME":"Date","OTHER":"Other","SCANNED_QR_CODES":"Scanned QR codes","SCAN_QR_CODE":"Read QR code","TRADE_NAME":"Name","UNKNOWN":"Unknown","USER_INFORMATION":"User Information","VACCINATION_PROOF":"COVID-19 Immunization Record","VACCINE":"Vaccine","VACCINE_CODE":"Code","VACCINE_LIST":"List of Vaccines Administered","WRONG_QR_CODE":"Wrong QR code scanned. Try a different QR code."},"FOOTER":{"AKINOX_SOLUTIONS":"\xa9 2021 Akinox Solutions","PRIVACY_POLICIES":"Privacy policies"},"HEADER":{"CHANGE_LANGUAGE":"Fran\xe7ais","TITLE":"Vaccine Proof Reader"}}'
      );
    },
    217: function (e) {
      e.exports = JSON.parse(
        '{"APP":{"BIRTHDATE":"Date de naissance","DEVICE_LOAD_ERROR":"Erreur lors du chargement de la cam\xe9ra. Veuillez r\xe9initialiser l\'application.","FEMALE":"F\xe9minin","GENDER":"Sexe","LOCATION":"Lieu de vaccination","LOT_NUMBER":"Lot","MALE":"Masculin","NAME":"Nom","OCCURRENCE_DATE_TIME":"Date","OTHER":"Autre","SCANNED_QR_CODES":"Codes QR lus","SCAN_QR_CODE":"Lire le code QR","TRADE_NAME":"Nom","UNKNOWN":"Inconnu","USER_INFORMATION":"Informations sur l\'usager","VACCINATION_PROOF":"Preuve de vaccination COVID-19","VACCINE":"Vaccin","VACCINE_CODE":"Code","VACCINE_LIST":"Liste des vaccins administr\xe9s","WRONG_QR_CODE":"Mauvais code QR lu. Essayez un autre code QR."},"FOOTER":{"AKINOX_SOLUTIONS":"\xa9 2021 Akinox Solutions","PRIVACY_POLICIES":"Politiques de confidentialit\xe9"},"HEADER":{"CHANGE_LANGUAGE":"English","TITLE":"Lecteur de preuve de vaccination"}}'
      );
    },
    226: function (e, t, n) {},
    227: function (e, t, n) {},
    233: function (e, t, n) {},
    241: function (e, t, n) {},
    373: function (e, t) {},
    408: function (e, t) {},
    410: function (e, t) {},
    453: function (e, t, n) {},
    454: function (e, t, n) {
      "use strict";
      n.r(t);
      n(226), n(227), n(228);
      var r = n(123),
        i = n(45),
        c = { en: { translation: n(216) }, fr: { translation: n(217) } };
      r.a
        .use(i.e)
        .init({
          resources: c,
          lng: (navigator.language || navigator.userLanguage).split("-")[0],
          fallbackLng: "fr",
          keySeparator: ".",
          interpolation: { escapeValue: !1 },
        });
      r.a;
      var o,
        a = n(2),
        s = n.n(a),
        l = n(71),
        d = n.n(l),
        u = (n(233), n(220)),
        b = n(74),
        h = n(455),
        j = n(0),
        f = function () {
          var e = Object(h.a)(),
            t = e.t,
            n = e.i18n;
          return Object(j.jsx)("footer", {
            className: "mt-auto",
            role: "contentinfo",
            id: "piv_piedPage",
            children: Object(j.jsxs)("div", {
              className: "container mt-3",
              children: [
                Object(j.jsx)("div", {
                  className: "row",
                  children: Object(j.jsx)("div", {
                    className: "col",
                    children: Object(j.jsx)("img", {
                      alt: "Logo Akinox.",
                      src: "/img/AkinoxLogo.svg",
                      height: "25",
                    }),
                  }),
                }),
                Object(j.jsx)("div", {
                  className: "row mt-2",
                  children: Object(j.jsx)("div", {
                    className: "col",
                    children: Object(j.jsx)("small", {
                      children: t("FOOTER.AKINOX_SOLUTIONS"),
                    }),
                  }),
                }),
                Object(j.jsx)("div", {
                  className: "d-flex justify-content-center mb-2",
                  children: Object(j.jsx)("div", {
                    children: Object(j.jsx)("small", {
                      children: Object(j.jsx)("button", {
                        onClick: function () {
                          switch (n.language) {
                            case "en":
                              window.open(
                                "https://www.akinox.com/privacy-policy/ ",
                                "_blank"
                              );
                              break;
                            case "fr":
                              window.open(
                                "https://www.akinox.com/fr/politique-de-confidentialite/ ",
                                "_blank"
                              );
                          }
                        },
                        children: t("FOOTER.PRIVACY_POLICIES"),
                      }),
                    }),
                  }),
                }),
              ],
            }),
          });
        },
        O = function () {
          var e = Object(h.a)(),
            t = e.t,
            n = e.i18n;
          return Object(j.jsx)("div", {
            className: "contenu-fluide piv",
            children: Object(j.jsxs)("div", {
              className: "contenu-fixe",
              children: [
                Object(j.jsxs)("div", {
                  className: "ligne",
                  id: "entetePiv",
                  children: [
                    Object(j.jsx)("div", {
                      className: "d-flex align-items-center zone1",
                      children: Object(j.jsx)("img", {
                        id: "pivImage",
                        alt: "Logo du gouvernement du Qu\xe9bec.",
                        src: "/img/QUEBEC_blanc.svg",
                        width: "256",
                        height: "72",
                      }),
                    }),
                    Object(j.jsx)("div", {
                      className: "d-flex align-items-center zone2",
                      lang: "fr",
                      children: Object(j.jsx)("div", {
                        className: "identite desktop",
                        children: t("HEADER.TITLE"),
                      }),
                    }),
                    Object(j.jsx)("div", {
                      className:
                        "d-flex justify-content-end align-items-center zone3",
                      children: Object(j.jsx)("div", {
                        className: "zoneMenu",
                        children: Object(j.jsx)("button", {
                          onClick: function () {
                            switch (n.language) {
                              case "en":
                                n.changeLanguage("fr");
                                break;
                              case "fr":
                                n.changeLanguage("en");
                            }
                          },
                          className: "btn btn-link text-white",
                          children: t("HEADER.CHANGE_LANGUAGE"),
                        }),
                      }),
                    }),
                  ],
                }),
                Object(j.jsx)("div", {
                  className: "row",
                  id: "entetePiv--titreMobile",
                  children: Object(j.jsx)("div", {
                    className: "titreM mobile",
                    children: Object(j.jsx)("p", {
                      children: t("HEADER.TITLE"),
                    }),
                  }),
                }),
              ],
            }),
          });
        },
        m = n(10),
        v = n.n(m),
        p = n(25),
        g = (n(241), n(225)),
        C = n(7),
        E = n(73),
        N = n(8),
        x = (n(242), n(121)),
        A = n.n(x),
        w = n(3),
        R = n(122),
        I = n.n(R),
        T = n(222),
        P = n.n(T),
        D = n(223);
      !(function (e) {
        (e.Patient = "Patient"), (e.Immunization = "Immunization");
      })(o || (o = {}));
      var L,
        _,
        y,
        S,
        k,
        z,
        q,
        M,
        V,
        U,
        Q,
        F,
        B,
        W,
        G,
        H,
        J,
        K,
        Y = n(224),
        X = n.n(Y),
        $ = function e() {
          var t = this;
          Object(E.a)(this, e),
            (this.baseAxiosInstance = void 0),
            (this.fetchPublicKey = function () {
              var e,
                n =
                  null !== (e = "/public/jwks.json") ? e : "/public/jwks.json";
              return t.baseAxiosInstance.get(n);
            }),
            (this.baseAxiosInstance = X.a.create({
              baseURL: "",
              timeout: 6e4,
            }));
        },
        Z = n(452),
        ee =
          ((L = function e() {
            var t = this;
            Object(E.a)(this, e),
              Object(C.a)(this, "qrCodes", _, this),
              Object(C.a)(this, "qrCodesAmount", y, this),
              Object(C.a)(this, "scannedQrCodesCount", S, this),
              Object(C.a)(this, "scanning", k, this),
              Object(C.a)(this, "scanningError", z, this),
              Object(C.a)(this, "patient", q, this),
              Object(C.a)(this, "immunizations", M, this),
              Object(C.a)(this, "deviceAccess", V, this),
              Object(C.a)(this, "videoDeviceLoadError", U, this),
              (this.controls = null),
              (this.qrCodeService = new $()),
              Object(C.a)(this, "getDeviceAccess", Q, this),
              Object(C.a)(this, "startReader", F, this),
              Object(C.a)(this, "handleScan", B, this),
              (this.handleError = function (e) {
                e && console.error(e),
                  t.resetScanning(),
                  (t.scanningError = !0);
              }),
              Object(C.a)(this, "generateJwsFromChunks", W, this),
              Object(C.a)(this, "decodeQrCodeChange", G, this),
              Object(C.a)(this, "parseInflatedPayload", H, this),
              Object(C.a)(this, "resetScanning", J, this),
              Object(w.e)(this);
          }),
          (_ = Object(N.a)(L.prototype, "qrCodes", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return [];
            },
          })),
          (y = Object(N.a)(L.prototype, "qrCodesAmount", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return 0;
            },
          })),
          (S = Object(N.a)(L.prototype, "scannedQrCodesCount", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return 0;
            },
          })),
          (k = Object(N.a)(L.prototype, "scanning", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return !0;
            },
          })),
          (z = Object(N.a)(L.prototype, "scanningError", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return !1;
            },
          })),
          (q = Object(N.a)(L.prototype, "patient", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return {};
            },
          })),
          (M = Object(N.a)(L.prototype, "immunizations", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return [];
            },
          })),
          (V = Object(N.a)(L.prototype, "deviceAccess", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return !1;
            },
          })),
          (U = Object(N.a)(L.prototype, "videoDeviceLoadError", [w.f], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              return !1;
            },
          })),
          (Q = Object(N.a)(L.prototype, "getDeviceAccess", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return Object(p.a)(
                v.a.mark(function t() {
                  return v.a.wrap(function (t) {
                    for (;;)
                      switch ((t.prev = t.next)) {
                        case 0:
                          navigator.mediaDevices
                            .getUserMedia({ video: !0 })
                            .then(
                              Object(p.a)(
                                v.a.mark(function t() {
                                  return v.a.wrap(function (t) {
                                    for (;;)
                                      switch ((t.prev = t.next)) {
                                        case 0:
                                          return (
                                            (t.next = 2),
                                            navigator.mediaDevices.enumerateDevices()
                                          );
                                        case 2:
                                          t.sent && (e.deviceAccess = !0);
                                        case 4:
                                        case "end":
                                          return t.stop();
                                      }
                                  }, t);
                                })
                              )
                            );
                        case 1:
                        case "end":
                          return t.stop();
                      }
                  }, t);
                })
              );
            },
          })),
          (F = Object(N.a)(L.prototype, "startReader", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return (function () {
                var t = Object(p.a)(
                  v.a.mark(function t(n) {
                    var r;
                    return v.a.wrap(function (t) {
                      for (;;)
                        switch ((t.prev = t.next)) {
                          case 0:
                            if (
                              ((r = new D.BrowserQRCodeReader()),
                              e.deviceAccess)
                            ) {
                              t.next = 4;
                              break;
                            }
                            return (t.next = 4), e.getDeviceAccess();
                          case 4:
                            navigator.mediaDevices
                              .getUserMedia({ video: !0 })
                              .then(
                                Object(p.a)(
                                  v.a.mark(function t() {
                                    return v.a.wrap(function (t) {
                                      for (;;)
                                        switch ((t.prev = t.next)) {
                                          case 0:
                                            if (!e.deviceAccess || !n) {
                                              t.next = 4;
                                              break;
                                            }
                                            return (
                                              (t.next = 3),
                                              r
                                                .decodeFromVideoDevice(
                                                  void 0,
                                                  n,
                                                  function (t, n, r) {
                                                    t &&
                                                      e.handleScan(t.getText());
                                                  }
                                                )
                                                .catch(function (t) {
                                                  return (
                                                    console.error(t),
                                                    (e.videoDeviceLoadError =
                                                      !0),
                                                    null
                                                  );
                                                })
                                            );
                                          case 3:
                                            e.controls = t.sent;
                                          case 4:
                                          case "end":
                                            return t.stop();
                                        }
                                    }, t);
                                  })
                                )
                              );
                          case 5:
                          case "end":
                            return t.stop();
                        }
                    }, t);
                  })
                );
                return function (e) {
                  return t.apply(this, arguments);
                };
              })();
            },
          })),
          (B = Object(N.a)(L.prototype, "handleScan", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return function (t) {
                if (t && !e.qrCodes.includes(t)) {
                  var n = t.split("/");
                  if (
                    (0 === e.qrCodesAmount &&
                      (e.qrCodesAmount = n.length > 2 ? +n[2] : 1),
                    e.qrCodesAmount > 1 &&
                      (+n.length < 2 || e.qrCodesAmount !== +n[2]))
                  )
                    return void (e.scanningError = !0);
                  (e.qrCodes = [].concat(Object(g.a)(e.qrCodes), [t])),
                    e.scannedQrCodesCount++,
                    (e.qrCodesAmount < 2 ||
                      e.qrCodesAmount === e.qrCodes.length) &&
                      e.generateJwsFromChunks();
                }
              };
            },
          })),
          (W = Object(N.a)(L.prototype, "generateJwsFromChunks", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return function () {
                for (
                  var t = "",
                    n = function (n) {
                      var r =
                          e.qrCodes.length > 1
                            ? e.qrCodes.find(function (e) {
                                return (
                                  +(null === e || void 0 === e
                                    ? void 0
                                    : e.split("/")[1]) === n
                                );
                              })
                            : e.qrCodes[0],
                        i = e.qrCodes.length > 1 ? 3 : 1,
                        c =
                          null === r || void 0 === r ? void 0 : r.split("/")[i];
                      c &&
                        (t += (function (e) {
                          for (var t = "", n = 0; n < e.length; n += 2)
                            t += String.fromCharCode(
                              +e.substring(n, n + 2) + 45
                            );
                          return t;
                        })(c));
                    },
                    r = 1;
                  r <= e.qrCodes.length;
                  r++
                )
                  n(r);
                e.decodeQrCodeChange(t);
              };
            },
          })),
          (G = Object(N.a)(L.prototype, "decodeQrCodeChange", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return (function () {
                var t = Object(p.a)(
                  v.a.mark(function t(n) {
                    var r, i, c, o, a;
                    return v.a.wrap(
                      function (t) {
                        for (;;)
                          switch ((t.prev = t.next)) {
                            case 0:
                              return (
                                (t.prev = 0),
                                (t.next = 3),
                                e.qrCodeService.fetchPublicKey()
                              );
                            case 3:
                              return (
                                (r = t.sent),
                                (i = r.data.keys),
                                (c = I.a.JWK.createKeyStore()),
                                (o = i.length ? i[0] : {}),
                                (a = JSON.parse(A.a.decode(n.split(".")[0]))),
                                (o.kid = a.kid),
                                (t.next = 11),
                                c.add(o)
                              );
                            case 11:
                              I.a.JWS.createVerify(c)
                                .verify(n)
                                .then(function (t) {
                                  P.a.inflateRaw(t.payload, function (t, n) {
                                    Object(w.g)(function () {
                                      var t;
                                      (e.scanning = !1),
                                        (e.scanningError = !1),
                                        null === (t = e.controls) ||
                                          void 0 === t ||
                                          t.stop(),
                                        e.parseInflatedPayload(n);
                                    });
                                  });
                                })
                                .catch(function (t) {
                                  e.handleError(t);
                                }),
                                (t.next = 17);
                              break;
                            case 14:
                              (t.prev = 14),
                                (t.t0 = t.catch(0)),
                                e.handleError(t.t0);
                            case 17:
                            case "end":
                              return t.stop();
                          }
                      },
                      t,
                      null,
                      [[0, 14]]
                    );
                  })
                );
                return function (e) {
                  return t.apply(this, arguments);
                };
              })();
            },
          })),
          (H = Object(N.a)(L.prototype, "parseInflatedPayload", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return function (t) {
                var n = JSON.parse(t.toString("utf8")).vc.credentialSubject
                    .fhirBundle.entry,
                  r = n.find(function (e) {
                    return e.resource.resourceType === o.Patient;
                  });
                  console.log(r);
                  console.log('Bonjour ' + r.resource.name[0].given[0] + ' ' + r.resource.name[0].family[0]);
                r &&
                  (e.patient = {
                    name: r.resource.name
                      .map(function (e) {
                        return e.family + ", " + e.given;
                      })
                      .join(". "),
                    birthdate: r.resource.birthDate,
                    gender: r.resource.gender,
                  });
                var i = n.filter(function (e) {
                  return e.resource.resourceType === o.Immunization;
                });
                (i = i.sort(function (e, t) {
                  return (
                    new Date(t.resource.occurrenceDateTime).getTime() -
                    new Date(e.resource.occurrenceDateTime).getTime()
                  );
                })).forEach(function (t) {
                  var n,
                    r = (
                      null === (n = t.resource.note) || void 0 === n
                        ? void 0
                        : n.length
                    )
                      ? t.resource.note[0].text
                      : "",
                    i = t.resource.vaccineCode.coding
                      ? t.resource.vaccineCode.coding[0].code
                      : "";
                  e.immunizations.push({
                    covidVaccineCode: { code: i, tradeName: r },
                    lotNumber: t.resource.lotNumber,
                    occurrenceDateTime: Z(t.resource.occurrenceDateTime).format(
                      "YYYY-MM-DD"
                    ),
                    location: t.resource.location.display,
                  });
                });
              };
            },
          })),
          (J = Object(N.a)(L.prototype, "resetScanning", [w.b], {
            configurable: !0,
            enumerable: !0,
            writable: !0,
            initializer: function () {
              var e = this;
              return function () {
                (e.qrCodes = []),
                  (e.qrCodesAmount = 0),
                  (e.scannedQrCodesCount = 0),
                  (e.scanning = !0),
                  (e.patient = {}),
                  (e.immunizations = []);
              };
            },
          })),
          L),
        te = Object(a.createContext)(new ee());
      !(function (e) {
        (e[(e.TopLeft = 1)] = "TopLeft"),
          (e[(e.TopRight = 2)] = "TopRight"),
          (e[(e.BottomLeft = 3)] = "BottomLeft"),
          (e[(e.BottomRight = 4)] = "BottomRight");
      })(K || (K = {}));
      n(453);
      var ne,
        re = function (e) {
          return Object(j.jsxs)(s.a.Fragment, {
            children: [
              Object(j.jsx)("div", {
                className: "corner ".concat(
                  (function (e) {
                    switch (e) {
                      case K.TopLeft:
                        return "top-corner horizontal-corner left-horizontal-corner";
                      case K.BottomLeft:
                        return "bottom-horizontal-corner horizontal-corner left-horizontal-corner";
                      case K.TopRight:
                        return "top-corner horizontal-corner right-horizontal-corner";
                      case K.BottomRight:
                        return "bottom-horizontal-corner horizontal-corner right-horizontal-corner";
                    }
                  })(e.position)
                ),
              }),
              Object(j.jsx)("div", {
                className: "corner ".concat(
                  (function (e) {
                    switch (e) {
                      case K.TopLeft:
                        return "top-corner vertical-corner left-vertical-corner";
                      case K.BottomLeft:
                        return "vertical-corner bottom-vertical-corner left-vertical-corner";
                      case K.TopRight:
                        return "top-corner vertical-corner right-vertical-corner";
                      case K.BottomRight:
                        return "vertical-corner bottom-vertical-corner right-vertical-corner";
                    }
                  })(e.position)
                ),
              }),
            ],
          });
        },
        ie = Object(b.a)(function () {
          var e = Object(a.useContext)(te);
          Object(a.useEffect)(function () {
            t();
          });
          var t = (function () {
            var t = Object(p.a)(
              v.a.mark(function t() {
                var n, r;
                return v.a.wrap(function (t) {
                  for (;;)
                    switch ((t.prev = t.next)) {
                      case 0:
                        (r =
                          null !==
                            (n = document.querySelector(
                              "#qr-code-webcam-area > video"
                            )) && void 0 !== n
                            ? n
                            : void 0),
                          e.startReader(r);
                      case 2:
                      case "end":
                        return t.stop();
                    }
                }, t);
              })
            );
            return function () {
              return t.apply(this, arguments);
            };
          })();
          return e.deviceAccess
            ? Object(j.jsx)("div", {
                className: "d-flex justify-content-center pt-4",
                children: Object(j.jsx)("section", {
                  className: "qr-reader-container",
                  children: Object(j.jsxs)("section", {
                    id: "qr-code-webcam-area",
                    className: "qr-reader",
                    children: [
                      Object(j.jsxs)("div", {
                        className: "video-overlay",
                        children: [
                          Object(j.jsx)(re, { position: K.TopLeft }),
                          Object(j.jsx)(re, { position: K.BottomLeft }),
                          Object(j.jsx)(re, { position: K.TopRight }),
                          Object(j.jsx)(re, { position: K.BottomRight }),
                        ],
                      }),
                      Object(j.jsx)("video", { playsInline: !0 }),
                    ],
                  }),
                }),
              })
            : null;
        });
      !(function (e) {
        (e.Female = "Female"),
          (e.Male = "Male"),
          (e.Other = "Other"),
          (e.Unknown = "Unknown");
      })(ne || (ne = {}));
      var ce = Object(b.a)(function () {
          var e,
            t = Object(a.useContext)(te),
            n = Object(h.a)().t;
          return (
            new $().fetchPublicKey(),
            Object(j.jsxs)("div", {
              className: "App d-flex flex-column min-vh-100",
              children: [
                Object(j.jsx)(O, {}),
                Object(j.jsx)("div", {
                  className: "container",
                  children: Object(j.jsx)("div", {
                    className: "row",
                    children: Object(j.jsxs)("div", {
                      className: "col-sm",
                      children: [
                        Object(j.jsx)("div", {
                          children: Object(j.jsx)("button", {
                            className: "btn btn-primary",
                            onClick: t.resetScanning,
                            children: n("APP.SCAN_QR_CODE"),
                          }),
                        }),
                        t.scanning
                          ? Object(j.jsxs)("div", {
                              children: [
                                Object(j.jsx)(ie, {}),
                                t.videoDeviceLoadError &&
                                  Object(j.jsxs)("span", {
                                    className: "text-danger",
                                    children: [
                                      Object(j.jsx)("br", {}),
                                      n("APP.DEVICE_LOAD_ERROR"),
                                    ],
                                  }),
                                t.qrCodesAmount > 1 &&
                                  Object(j.jsx)("span", {
                                    children:
                                      n("APP.SCANNED_QR_CODES") +
                                      ": " +
                                      t.scannedQrCodesCount +
                                      "/" +
                                      t.qrCodesAmount,
                                  }),
                                t.scanningError &&
                                  Object(j.jsxs)("span", {
                                    className: "text-danger",
                                    children: [
                                      Object(j.jsx)("br", {}),
                                      n("APP.WRONG_QR_CODE"),
                                    ],
                                  }),
                              ],
                            })
                          : Object(j.jsxs)("div", {
                              className: "text-left card mt-4",
                              children: [
                                Object(j.jsx)("h5", {
                                  className: "card-header",
                                  children: n("APP.VACCINATION_PROOF"),
                                }),
                                Object(j.jsxs)("div", {
                                  className: "card-body",
                                  children: [
                                    Object(j.jsx)("h5", {
                                      className: "mt-4 font-weight-bold",
                                      children: n("APP.USER_INFORMATION"),
                                    }),
                                    Object(j.jsx)("hr", {}),
                                    Object(j.jsxs)("dl", {
                                      className: "row mt-3",
                                      children: [
                                        Object(j.jsx)("dt", {
                                          className: "col-sm-3",
                                          children: n("APP.NAME"),
                                        }),
                                        Object(j.jsx)("dd", {
                                          className: "col-sm-9",
                                          children:
                                            (null === (e = t.patient) ||
                                            void 0 === e
                                              ? void 0
                                              : e.name) &&
                                            Object(j.jsx)("span", {
                                              children: t.patient.name,
                                            }),
                                        }),
                                        Object(j.jsx)("dt", {
                                          className: "col-sm-3",
                                          children: n("APP.GENDER"),
                                        }),
                                        Object(j.jsx)("dd", {
                                          className: "col-sm-9",
                                          children: t.patient.gender
                                            ? n(
                                                "APP." +
                                                  ne[
                                                    t.patient.gender
                                                  ].toUpperCase()
                                              )
                                            : "",
                                        }),
                                        Object(j.jsx)("dt", {
                                          className: "col-sm-3",
                                          children: n("APP.BIRTHDATE"),
                                        }),
                                        Object(j.jsx)("dd", {
                                          className: "col-sm-9",
                                          children: t.patient.birthdate,
                                        }),
                                      ],
                                    }),
                                    Object(j.jsx)("h5", {
                                      className: "mt-5 font-weight-bold",
                                      children: n("APP.VACCINE_LIST"),
                                    }),
                                    Object(j.jsxs)("table", {
                                      className: "table mt-3",
                                      children: [
                                        Object(j.jsx)("thead", {
                                          children: Object(j.jsxs)("tr", {
                                            children: [
                                              Object(j.jsx)("th", {
                                                style: { minWidth: "200px" },
                                                scope: "col",
                                                children: n("APP.VACCINE"),
                                              }),
                                              Object(j.jsx)("th", {
                                                scope: "col",
                                                children: n("APP.LOCATION"),
                                              }),
                                            ],
                                          }),
                                        }),
                                        Object(j.jsx)("tbody", {
                                          children: t.immunizations.map(
                                            function (e) {
                                              var t, r;
                                              return Object(j.jsxs)(
                                                "tr",
                                                {
                                                  children: [
                                                    Object(j.jsxs)("td", {
                                                      children: [
                                                        n("APP.TRADE_NAME") +
                                                          ": " +
                                                          (null ===
                                                            (t =
                                                              e.covidVaccineCode) ||
                                                          void 0 === t
                                                            ? void 0
                                                            : t.tradeName),
                                                        Object(j.jsx)("br", {}),
                                                        n("APP.VACCINE_CODE") +
                                                          ": " +
                                                          (null ===
                                                            (r =
                                                              e.covidVaccineCode) ||
                                                          void 0 === r
                                                            ? void 0
                                                            : r.code),
                                                        Object(j.jsx)("br", {}),
                                                        n("APP.LOT_NUMBER") +
                                                          ": " +
                                                          e.lotNumber,
                                                        Object(j.jsx)("br", {}),
                                                        n(
                                                          "APP.OCCURRENCE_DATE_TIME"
                                                        ) +
                                                          ": " +
                                                          e.occurrenceDateTime,
                                                      ],
                                                    }),
                                                    Object(j.jsx)("td", {
                                                      children: e.location,
                                                    }),
                                                  ],
                                                },
                                                u.uniqueId()
                                              );
                                            }
                                          ),
                                        }),
                                      ],
                                    }),
                                  ],
                                }),
                              ],
                            }),
                      ],
                    }),
                  }),
                }),
                Object(j.jsx)(f, {}),
              ],
            })
          );
        }),
        oe = function (e) {
          e &&
            e instanceof Function &&
            n
              .e(3)
              .then(n.bind(null, 456))
              .then(function (t) {
                var n = t.getCLS,
                  r = t.getFID,
                  i = t.getFCP,
                  c = t.getLCP,
                  o = t.getTTFB;
                n(e), r(e), i(e), c(e), o(e);
              });
        },
        ae = Boolean(
          "localhost" === window.location.hostname ||
            "[::1]" === window.location.hostname ||
            window.location.hostname.match(
              /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
            )
        );
      function se(e, t) {
        navigator.serviceWorker
          .register(e)
          .then(function (e) {
            e.onupdatefound = function () {
              var n = e.installing;
              null != n &&
                (n.onstatechange = function () {
                  "installed" === n.state &&
                    (navigator.serviceWorker.controller
                      ? (console.log(
                          "New content is available and will be used when all tabs for this page are closed. See https://cra.link/PWA."
                        ),
                        t && t.onUpdate && t.onUpdate(e))
                      : (console.log("Content is cached for offline use."),
                        t && t.onSuccess && t.onSuccess(e)));
                });
            };
          })
          .catch(function (e) {
            console.error("Error during service worker registration:", e);
          });
      }
      d.a.render(
        Object(j.jsx)(s.a.StrictMode, { children: Object(j.jsx)(ce, {}) }),
        document.getElementById("root")
      ),
        (function (e) {
          if ("serviceWorker" in navigator) {
            if (
              new URL("", window.location.href).origin !==
              window.location.origin
            )
              return;
            window.addEventListener("load", function () {
              var t = "".concat("", "/service-worker.js");
              ae
                ? (!(function (e, t) {
                    fetch(e, { headers: { "Service-Worker": "script" } })
                      .then(function (n) {
                        var r = n.headers.get("content-type");
                        404 === n.status ||
                        (null != r && -1 === r.indexOf("javascript"))
                          ? navigator.serviceWorker.ready.then(function (e) {
                              e.unregister().then(function () {
                                window.location.reload();
                              });
                            })
                          : se(e, t);
                      })
                      .catch(function () {
                        console.log(
                          "No internet connection found. App is running in offline mode."
                        );
                      });
                  })(t, e),
                  navigator.serviceWorker.ready.then(function () {
                    console.log(
                      "This web app is being served cache-first by a service worker. To learn more, visit https://cra.link/PWA"
                    );
                  }))
                : se(t, e);
            });
          }
        })(),
        oe();
    },
    98: function (e, t) {},
  },
  [[454, 1, 2]],
]);
//# sourceMappingURL=main.47f52f01.chunk.js.map
