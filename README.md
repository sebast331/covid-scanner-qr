# Description
Application pour scanner les code QR de Covid-19 du Québec

Source : https://scanner.aki-vacc-ca-dev.akinox.dev

# Lancer l'Application
```bash
python3 -m http.server 8000
```
et naviguer à l'adresse http://localhost:8000
